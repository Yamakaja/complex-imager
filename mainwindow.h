#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QApplication &application, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();

    void on_rangeImaginaryMax_valueChanged(double arg1);

    void on_rangeRealMin_valueChanged(double arg1);

    void on_rangeRealMax_valueChanged(double arg1);

    void on_rangeImaginaryMin_valueChanged(double arg1);

    void on_renderTarget_updatePosition(float, float, float, float);

    void on_adjustToScreenButton_clicked();

private:
    Ui::MainWindow *ui;
    QApplication &application;

    void updateRender();
};

#endif // MAINWINDOW_H
