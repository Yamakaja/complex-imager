#ifndef RENDERWIDGET_HPP
#define RENDERWIDGET_HPP

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <string>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)

namespace Ui {
class RenderWidget;
}

class RenderWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    RenderWidget(QWidget *parent = 0);
    ~RenderWidget();

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    void updateRange(float minX, float maxX, float minY, float maxY, bool update = false);

    float getMinX() { return this->minX; }
    float getMaxX() { return this->maxX; }
    float getMinY() { return this->minY; }
    float getMaxY() { return this->maxY; }

public slots:
    void cleanup();

signals:
    void updatePosition(float minX, float maxX, float minY, float maxY);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    QOpenGLShaderProgram *m_program;
    int dragStartX;
    int dragStartY;

    float minX, maxX, minY, maxY;
};

#endif // RENDERWIDGET_HPP
