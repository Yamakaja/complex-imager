# ComplexImager

Contradictory to the title, this visualizer isn't complicated, but rather works with complex numbers

## Build

Make sure you have a recent version of GCC (G++) and qmake (5.9+) installed, then execute the build script:

    ./build.sh

