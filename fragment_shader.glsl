#version 130
#define M_PI 3.1415926535897932384626433832795

out vec4 color;

uniform int viewWidth;
uniform int viewHeight;

uniform float minX;
uniform float maxX;

uniform float minY;
uniform float maxY;

float map(float value, float minSource, float maxSource, float minTarget, float maxTarget) {
    return ((value - minSource) / (maxSource - minSource)) * (maxTarget - minTarget) + minTarget;
}

vec2 product(vec2 a, vec2 b) {
    return vec2(a.x*b.x-a.y*b.y, a.x*b.y+a.y*b.x);
}

float length(vec2 a) {
    return sqrt(a.x * a.x + a.y * a.y);
}

float distance(vec2 a, vec2 b) {
    float diffX = a.x - b.x;
    float diffY = a.y - b.y;
    return sqrt(diffX * diffX + diffY * diffY);
}

/**
  * Returns the angle of the passed vector in radians. Range: [0;2PI[
  */
float angle(vec2 a) {
    float ret;
    if (a.x >= 0)
        ret = asin(a.y / length(a));
    else
        ret = M_PI - asin(a.y / length(a));

    if (ret < 0)
        ret = ret + 2 * M_PI;

    return ret;
}

/**
  * Returns the angle of the passed vector in radians, ignoring the x value. Range: [-PI/2;PI/2]
  */
float lazyAngle(vec2 a) {
    return asin(a.y / length(a));
}

void main() {
    vec2 pos = vec2(map(gl_FragCoord.x, 0, viewWidth, minX, maxX), map(gl_FragCoord.y, 0, viewHeight, minY, maxY));

    vec2 prod = product(pos, pos);

    float angle = lazyAngle(prod) + 0.5 * M_PI;

    color = vec4(angle / M_PI, sin(prod.x / 8), sin(prod.y / 8), 1); // Arguments: red, green, blue, alpha
}
