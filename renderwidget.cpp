#include "renderwidget.hpp"

#include "QMouseEvent"
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <QString>
#include <QMessageBox>

#include <fstream>
#include <streambuf>
#include <math.h>
#include <iostream>


RenderWidget::RenderWidget(QWidget *parent) : QOpenGLWidget(parent), minX(-2), maxX(2), minY(-2), maxY(2) {
}

RenderWidget::~RenderWidget() {
    cleanup();
}

QSize RenderWidget::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize RenderWidget::sizeHint() const {
    return QSize(400, 400);
}

void RenderWidget::updateRange(float minX, float maxX, float minY, float maxY, bool update) {
    m_program->bind();

    m_program->setUniformValue("minX", minX);
    m_program->setUniformValue("maxX", maxX);
    m_program->setUniformValue("minY", minY);
    m_program->setUniformValue("maxY", maxY);

    m_program->release();

    this->minX = minX;
    this->maxX = maxX;
    this->minY = minY;
    this->maxY = maxY;

    if (update)
            this->updatePosition(minX, maxX, minY, maxY);

    this->update();
}

void RenderWidget::cleanup() {
    makeCurrent();
    doneCurrent();
}

void RenderWidget::initializeGL() {
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &RenderWidget::cleanup);

    initializeOpenGLFunctions();
    glClearColor(0, 0, 0, 1);

    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vertex_shader.glsl");
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fragment_shader.glsl");
    m_program->link();

    m_program->bind();
    m_program->setUniformValue("minX", minX);
    m_program->setUniformValue("maxX", maxX);
    m_program->setUniformValue("minY", minY);
    m_program->setUniformValue("maxY", maxY);
    m_program->release();
}

void RenderWidget::mousePressEvent(QMouseEvent *event) {
    this->dragStartX = event->x();
    this->dragStartY = event->y();
}

void RenderWidget::mouseMoveEvent(QMouseEvent *event) {
    float horizontalShiftPerPixel = (maxX - minX) / this->width();
    float verticalShiftPerPixel = -(maxY - minY) / this->height();

    float xShift = (this->dragStartX - event->x()) * horizontalShiftPerPixel;
    float yShift = (this->dragStartY - event->y()) * verticalShiftPerPixel;

    this->updateRange(this->minX + xShift, this->maxX + xShift, this->minY + yShift, this->maxY + yShift, true);

    this->dragStartX = event->x();
    this->dragStartY = event->y();
}

void RenderWidget::wheelEvent(QWheelEvent *event) {
    float factor = event->delta() < 0 ? 0.1 : (1/1.1) - 1;

    float width = maxX - minX;
    float height = maxY - minY;

    this->updateRange(this->minX - width * factor,
                      this->maxX + width * factor,
                      this->minY - height * factor,
                      this->maxY + height * factor,
                      true);
}

void RenderWidget::resizeGL(int width, int height) {
    m_program->bind();

    m_program->setUniformValue("viewWidth", width);
    m_program->setUniformValue("viewHeight", height);

    m_program->release();
}

void RenderWidget::paintGL() {
    m_program->bind();
    glClear(GL_COLOR_BUFFER_BIT);

    glRectf(-1.0f, -1.0f, 1.0f, 1.0f);

    QSize size = this->size();

    glFlush();
    m_program->release();
}
