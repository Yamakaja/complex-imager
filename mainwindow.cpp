#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSpinBox>

MainWindow::MainWindow(QApplication &application, QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), application(application) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::on_actionQuit_triggered() {
    application.quit();
}

void MainWindow::updateRender() {
    ui->renderTarget->updateRange(ui->rangeRealMin->value(),
                                  ui->rangeRealMax->value(),
                                  ui->rangeImaginaryMin->value(),
                                  ui->rangeImaginaryMax->value());
    ui->renderTarget->update();
}

void MainWindow::on_rangeImaginaryMax_valueChanged(double x) {
    this->updateRender();
}

void MainWindow::on_rangeRealMin_valueChanged(double x) {
    this->updateRender();
}

void MainWindow::on_rangeRealMax_valueChanged(double x) {
    this->updateRender();
}

void MainWindow::on_rangeImaginaryMin_valueChanged(double x) {
    this->updateRender();
}

void MainWindow::on_renderTarget_updatePosition(float minX, float maxX, float minY, float maxY) {
    ui->rangeRealMin->blockSignals(true);
    ui->rangeRealMin->setValue(minX);
    ui->rangeRealMin->blockSignals(false);

    ui->rangeRealMax->blockSignals(true);
    ui->rangeRealMax->setValue(maxX);
    ui->rangeRealMax->blockSignals(false);

    ui->rangeImaginaryMin->blockSignals(true);
    ui->rangeImaginaryMin->setValue(minY);
    ui->rangeImaginaryMin->blockSignals(false);

    ui->rangeImaginaryMax->blockSignals(true);
    ui->rangeImaginaryMax->setValue(maxY);
    ui->rangeImaginaryMax->blockSignals(false);
}

void MainWindow::on_adjustToScreenButton_clicked() {
    float height = ui->renderTarget->getMaxY() - ui->renderTarget->getMinY();
    float currentWidth = (ui->renderTarget->getMaxX() - ui->renderTarget->getMinX());
    float width = (static_cast<float>(ui->renderTarget->width()) / ui->renderTarget->height()) * height;

    float margin = (width - currentWidth) / 2;

    ui->renderTarget->updateRange(ui->renderTarget->getMinX() - margin,
                                  ui->renderTarget->getMaxX() + margin,
                                  ui->renderTarget->getMinY(),
                                  ui->renderTarget->getMaxY(),
                                  true);
}
